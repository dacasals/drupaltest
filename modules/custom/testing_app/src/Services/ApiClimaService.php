<?php

namespace Drupal\testing_app\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Driver\pgsql\Connection;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\views\Plugin\views\area\HTTPStatusCode;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiClimaService.
 */
class ApiClimaService
{

    /**
     * Drupal\Core\Database\Driver\pgsql\Connection definition.
     *
     * @var \Drupal\Core\Database\Driver\pgsql\Connection
     */
    protected $database;
    /**
     * GuzzleHttp\ClientInterface definition.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;
    protected $country_manager;
    /**
     * Drupal\Core\Config\ConfigFactoryInterface\ConfigFactoryInterface
     * @var ConfigFactoryInterface
     */
    protected $config_factory;

    /**
     * Constructs a new ApiClimaService object.
     */
    public function __construct(Connection $database, ClientInterface $http_client,
                                CountryManagerInterface $country_manager, ConfigFactoryInterface $configFactory)
    {
        $this->database = $database;
        $this->httpClient = $http_client;
        $this->country_manager = $country_manager;
        $this->config_factory = $configFactory;
    }

    public function getClimaDataByCity($city)
    {
        $endPoint = $this->config_factory->get('testing_app.config')->get('endpoint');
        $API_KEY = $this->config_factory->get('testing_app.config')->get('api_id');
        $show_properties = $this->config_factory->get('testing_app.config')->get('show_properties');
        try {
            $response = $this->httpClient->request('GET', $endPoint, array(
                'query' => array(
                    'q' => "$city",
                    'appid' => $API_KEY
                ),
                'proxy' => array(
                    'http' => 'http://10.42.0.1:3129', // Use this proxy with "http"
                    'https' => 'http://10.42.0.1:3129', // Use this proxy with "https",
                )
            ));
        } catch (\Exception $e) {
            return(json_encode(array('error' => $e->getMessage())));
        } catch (GuzzleException $e) {
            return(json_encode(array('error' => $e->getMessage())));
        }
        $allValues = json_decode($response->getBody(),true);
        $mainValues = $allValues['main'];
        //Checking if exist and is marked as true in configuration module.
        foreach ($mainValues as $key=> $value){
            if (!array_key_exists($key,$show_properties) || !$show_properties[$key]){
                unset($mainValues[$key]);
            }
        }
        return $mainValues;
    }

}
