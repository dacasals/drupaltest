<?php

namespace Drupal\testing_app\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ClimaController.
 */
class ClimaController extends ControllerBase {

  /**
   * Drupal\Core\Http\ClientFactory definition.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;
  /**
   * Drupal\Core\DependencyInjection\ContainerBuilder definition.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $serviceContainer;

  /**
   * Constructs a new ClimaController object.
   */
  public function __construct(ClientInterface $http_client_factory, ContainerInterface $service_container) {
    $this->httpClientFactory = $http_client_factory;
    $this->serviceContainer = $service_container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('service_container')
    );
  }

  /**
   * Getciudades.
   *
   * @return string
   *   Return Hello string.
   */
  public function getCiudades() {
    $cities = $this->serviceContainer->get('testing_app.default')->getCitiesByCountryCode();
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: getCiudades'. json_encode($cities))
    ];
  }
  /**
   * Getclimabyciudad.
   *
   * @return string
   *   Return Hello string.
   */
  public function getClimaByCiudad() {
      $response = $this->httpClientFactory->request('GET', 'http://api.openweathermap.org/data/2.5/weather', array(
          'query' => array(
              'q' => "London",
              'appid' => 'f7f726dac4af70b5f506886e2f8c7c70'
          ),
          'proxy' => array(
              'http' => 'http://10.42.0.1:3129', // Use this proxy with "http"
              'https' => 'http://10.42.0.1:3129', // Use this proxy with "https",
          )
      ));
        dump(json_decode($response->getBody()));
      return new JsonResponse(json_decode($response->getBody()));
  }
  /**
   * Getconfiguraciones.
   *
   * @return string
   *   Return Hello string.
   */
  public function loadCities() {
      $host = \Drupal::request()->getHost();
      $json = file_get_contents(__DIR__.'/cities.json');
      $json_data = json_decode($json,true);
      $databaseS = $this->serviceContainer->get('database');
      for($i = 0; $i< count($json_data);$i++){
          $query = $databaseS->insert('city_entity')
              ->fields([
                  'name' => $json_data[$i]['name'],
                  'code_country' => $json_data[$i]['country'],
                  'uuid' =>  rand ( 1, 1000000 ),
                  'langcode'=> 'es',
                  'user_id' => \Drupal::currentUser()->id(),
                  'status' => TRUE
              ])
              ->execute();
      }

      return [
      '#type' => 'markup',
      '#markup' => $this->t( count($json_data).' Ciudades cargadas')
    ];
  }

}
