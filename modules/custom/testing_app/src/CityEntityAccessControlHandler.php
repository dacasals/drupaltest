<?php

namespace Drupal\testing_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ciudad entity.
 *
 * @see \Drupal\testing_app\Entity\CityEntity.
 */
class CityEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\testing_app\Entity\CityEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ciudad entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ciudad entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ciudad entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ciudad entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ciudad entities');
  }

}
