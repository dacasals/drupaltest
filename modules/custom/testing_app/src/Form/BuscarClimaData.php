<?php

namespace Drupal\testing_app\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\testing_app\Services\ApiClimaService;

/**
 * Class BuscarClimaData.
 */
class BuscarClimaData extends FormBase
{

    /**
     * Drupal\testing_app\Services\ApiClimaService definition.
     *
     * @var \Drupal\testing_app\Services\ApiClimaService
     */
    protected $testingAppDefault;

    /**
     * Constructs a new BuscarClimaData object.
     */
    public function __construct(
        ApiClimaService $testing_app_default,
        TwigEnvironment $twigEnvironment
    )
    {
        $this->testingAppDefault = $testing_app_default;
        $this->twigEnvironment = $twigEnvironment;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('testing_app.default'),
            $container->get('twig')
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'buscar_clima_data';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['ciudad'] = [
            '#type' => 'entity_autocomplete',
            '#title' => $this->t('Ciudad'),
            '#description' => $this->t('Nombre de la ciudad en formato ISO'),
            '#target_type' => 'city_entity',
            '#weight' => '0',
            '#ajax' => array(
                'callback' => array($this, 'getCiudadesCallback'),
                'event' => 'autocompleteclose',
                'wrapper' => 'edit-user-list',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => t('Searching Users...'),
                ),
            )
        ];
//    $form['pais'] = [
//      '#type' => 'entity_autocomplete',
//      '#title' => $this->t('Pais'),
//      '#description' => $this->t('Codigo del pais'),
//      '#weight' => '0',
//        '#ajax' => array(
//            'callback' => array($this, 'getCiudadesCallback'),
//            'event' => 'autocompleteclose',
//            'wrapper' => 'edit-user-list',
//            'progress' => array(
//                'type' => 'throbber',
//                'message' => t('Searching Users...'),
//            ),
//        )
//    ];
        $form['result_response'] = [
            '#type' => 'container',
            '#attributes' => array(
                'id' => array(
                    'ciudades_list',
                ),
            ),
        ];
//      $form['result_response'] = [
//          '#type' => 'container',
//          '#attributes' => array(
//              'id' => array(
//                  'country_list',
//              ),
//          ),
//      ];
//        $form['submit'] = [
//            '#type' => 'submit',
//            '#value' => $this->t('Submit'),
//        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Display result.
        foreach ($form_state->getValues() as $key => $value) {
            drupal_set_message($key . ': ' . $value);
        }

    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCiudadesCallback(array &$form, FormStateInterface $form_state)
    {
        $entity = \Drupal::entityTypeManager()->getStorage('city_entity')->load($form_state->getValue('ciudad'));

        $data = $this->testingAppDefault->getClimaDataByCity($entity->getName());

        try {
            $result = $this->twigEnvironment->load(
                drupal_get_path('module', 'testing_app') . '/templates/climadata_template.html.twig');
            $ajaxResponse = new AjaxResponse();
            $ajaxResponse->addCommand(new HtmlCommand('#ciudades_list',
                $result->render(array('data'=>$data,'city'=>array('name'=> $entity->getName(),'code_country'=>$entity->getCountryCode())))));
            return $ajaxResponse;
        } catch (\Exception $exception) {
            $ajaxResponse = new AjaxResponse();
            $ajaxResponse->addCommand(new HtmlCommand('#ciudades_list', '<div>No se encontraron los datos.</div>'));
        }

    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

//    public function getCountryByCodeCallback(array &$form, FormStateInterface $form_state)
//    {
////        $entity = \Drupal::entityTypeManager()->getStorage('city_entity')->load($form_state->getValue('name'));
////        $data = $this->testingAppDefault->getCountryByCode($entity->getName());
////        $result = "";
////        foreach ($data as $index => $item) {
////            $result .= "<li><b>$index: </b><span>$item</span></li>";
////        }
////        $result = "<ul>$result</ul>";
////        $ajaxResponse = new AjaxResponse();
////        $ajaxResponse->addCommand(new HtmlCommand('#country_list', $result));
////        return $ajaxResponse;
//    }
}
