<?php

namespace Drupal\testing_app\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Driver\pgsql\Connection;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Database\Driver\pgsql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\pgsql\Connection
   */
  protected $database;
  /**
   * Constructs a new ConfigForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      Connection $database
    ) {
    parent::__construct($config_factory);
        $this->database = $database;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'testing_app.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('testing_app.config');
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Endpoint del api'),
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get('endpoint'),
    ];
    $form['api_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api ID'),
      '#description' => $this->t('Identificador del api'),
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get('api_id'),
    ];
    $form['show_properties'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Propiedades a mostrar'),
      '#description' => $this->t('Propiedades a devolver de cada elemento'),
      '#options' => ['temp' => $this->t('temp'), 'pressure' => $this->t('pressure'), 'humidity' => $this->t('humidity'), 'temp_min' => $this->t('temp_min'), 'temp_max' => $this->t('temp_max')],
      '#default_value' => $config->get('show_properties'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('testing_app.config')
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->set('api_id', $form_state->getValue('api_id'))
      ->set('show_properties', $form_state->getValue('show_properties'))
      ->save();
  }

}
