<?php

namespace Drupal\testing_app\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ciudad entities.
 *
 * @ingroup testing_app
 */
class CityEntityDeleteForm extends ContentEntityDeleteForm {


}
