<?php

namespace Drupal\testing_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ciudad entities.
 *
 * @ingroup testing_app
 */
interface CityEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ciudad name.
   *
   * @return string
   *   Name of the Ciudad.
   */
  public function getName();

  /**
   * Sets the Ciudad name.
   *
   * @param string $name
   *   The Ciudad name.
   *
   * @return \Drupal\testing_app\Entity\CityEntityInterface
   *   The called Ciudad entity.
   */
  public function setName($name);

  /**
   * Gets the Ciudad creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ciudad.
   */
  public function getCreatedTime();

  /**
   * Sets the Ciudad creation timestamp.
   *
   * @param int $timestamp
   *   The Ciudad creation timestamp.
   *
   * @return \Drupal\testing_app\Entity\CityEntityInterface
   *   The called Ciudad entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ciudad published status indicator.
   *
   * Unpublished Ciudad are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ciudad is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ciudad.
   *
   * @param bool $published
   *   TRUE to set this Ciudad to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\testing_app\Entity\CityEntityInterface
   *   The called Ciudad entity.
   */
  public function setPublished($published);

}
