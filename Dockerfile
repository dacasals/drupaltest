FROM nexus.prod.uci.cu/drupal:8.5-rc-apache

ENV http_proxy http://10.42.0.1:3129
ENV https_proxy http://10.42.0.1:3129


RUN echo 'deb http://download.jovenclub.cu/repos/debian/stretch/ stretch main contrib non-free' > /etc/apt/sources.list
RUN echo 'deb http://download.jovenclub.cu/repos/debian/stretch-security/ stretch/updates main contrib non-free' >> /etc/apt/sources.list
RUN echo 'deb http://download.jovenclub.cu/repos/debian/stretch-updates/ stretch-updates main contrib non-free'  >> /etc/apt/sources.list
RUN echo 'deb http://download.jovenclub.cu/repos/debian/stretch-multimedia/ stretch main non-free'  >> /etc/apt/sources.list
RUN echo 'deb http://download.jovenclub.cu/repos/debian/stretch-backports/ stretch-backports main contrib non-free'  >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y nano \
wget \
git

ENV http_proxy ""
ENV https_proxy ""